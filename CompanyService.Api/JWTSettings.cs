namespace CompanyService
{
    //Could be used to implement auth in the app
    public class JWTSettings
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
}