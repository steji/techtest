﻿using CompanyService.Api.Entities;
using CompanyService.Api.Models;
using CompanyService.Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Buffering;
using System;

namespace CompanyService.Api
{
    public class Startup
    {
        public static IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.Configure<JWTSettings>(Configuration.GetSection("JWTSettings"));

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<ICompanyServiceContext, CompanyServiceContext>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IEmployeeService, EmployeeService>();

            var connectionString = Configuration["ConnectionStrings:CompanyServiceDBConnectionString"];
            services.AddDbContext<CompanyServiceContext>(o => o.UseSqlServer(connectionString));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, CompanyServiceContext companyServiceContext)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler();

            //for testing
            companyServiceContext.Database.EnsureDeleted();
            companyServiceContext.Database.EnsureCreated();

            companyServiceContext.SeedData();

            app.UseStatusCodePages();

            InitialiseAutoMapper();

            //app.UseHttpsRedirection();
            //app.UseResponseBuffering();
            app.UseMvc();
        }

        public static void InitialiseAutoMapper()
        {
            try
            {
                AutoMapper.Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<Department, DepartmentDto>();
                    cfg.CreateMap<CreateUpdateDepartmentDto, Department>();
                    cfg.CreateMap<Employee, EmployeeDto>();
                    cfg.CreateMap<CreateUpdateEmployeeDto, Employee>();
                });
            }
            catch (InvalidOperationException)
            {
                //already initialised
            }
        }
    }
}
