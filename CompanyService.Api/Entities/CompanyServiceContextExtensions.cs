using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Internal;

namespace CompanyService.Api.Entities
{
    public static class CompanyServiceContextExtensions
    {
        public static void SeedData(this CompanyServiceContext context)
        {
            if (context.Departments.Any())
                return;

            var hr = new Department
            {
                Name = "HR",
                Location = "Emond's Field",
                Employees = new List<Employee>()
            };

            var finance = new Department
            {
                Name = "Finance",
                Location = "Tar Valon",
                Employees = new List<Employee>()
            };

            var departments = new List<Department>
            {
                hr, finance
            };

            var rand = new Employee
            {
                FirstName = "Rand",
                LastName = "Al'thor",
                JobTitle = "Shepherd",
                Department = hr
            };

            var moraine = new Employee 
            {
                FirstName = "Moraine",
                LastName = "Sedai",
                JobTitle = "Aes Sedai",
                Department = finance
            };

            var employees = new List<Employee>
            {
                rand, moraine
            };

            hr.Employees.Add(rand);
            finance.Employees.Add(moraine);

            context.Employees.AddRange(employees);
            context.Departments.AddRange(departments);

            context.SaveChanges();
        }   
    }
}
