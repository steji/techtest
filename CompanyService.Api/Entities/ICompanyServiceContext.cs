using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CompanyService.Api.Entities
{
    public interface ICompanyServiceContext {
        DbSet<Department> Departments { get; set; }
        DbSet<Employee> Employees { get; set; }
        int SaveChanges();
        EntityEntry Remove(object entity);
    }
}