using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CompanyService.Api.Entities
{
    public sealed class CompanyServiceContext : DbContext, ICompanyServiceContext
    {
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public CompanyServiceContext(DbContextOptions<CompanyServiceContext> options) : base(options)
        {
            Database.EnsureCreated();
            //Database.Migrate();
        }

        // protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {
        //     base.OnModelCreating(modelBuilder);

        //     foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
        //     {
        //         relationship.DeleteBehavior = DeleteBehavior.Restrict;
        //     }
        // }
    }
}