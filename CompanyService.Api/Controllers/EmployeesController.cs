﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CompanyService.Api.Entities;
using CompanyService.Api.Models;
using CompanyService.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace CompanyService.Api.Controllers
{
    [Route("api/employees")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        //GET api/employees
        [HttpGet]
        public ActionResult GetAll()
        {
            var employees = _employeeService.GetEmployees();

            return Ok(employees);
        }

        // GET api/employees/id
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            var employee = _employeeService.GetEmployee(id);

            if (employee == null)
                return NotFound();

            return Ok(employee);
        }

        // POST api/employees
        [HttpPost]
        public IActionResult Create([FromBody] CreateUpdateEmployeeDto dto)
        {
            if (dto == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var employeeId = _employeeService.CreateEmployee(dto);

            if (employeeId <= 0)
                return StatusCode(500, "There was an error");

            return Created("Get", new { id = employeeId });
        }

        // PUT api/employees/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] CreateUpdateEmployeeDto dto)
        {
            if (dto == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_employeeService.EmployeeExists(id))
                return NotFound();

            _employeeService.UpdateEmployee(id, dto);

            if (!_employeeService.Save())
                return StatusCode(500, "Something went wrong");

            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_employeeService.EmployeeExists(id))
                return NotFound();

            _employeeService.DeleteEmployee(id);

            if (!_employeeService.Save())
                return StatusCode(500, "Something went wrong");

            return NoContent();
        }
    }
}
