﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CompanyService.Api.Entities;
using CompanyService.Api.Models;
using CompanyService.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace CompanyService.Api.Controllers
{
    [Route("api/departments")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;
        public DepartmentsController(IDepartmentService departmentService)
        {
           _departmentService = departmentService;
        }

        //GET api/departments
        [HttpGet]
        public ActionResult GetAll()
        { 
            var departments = _departmentService.GetDepartments();

            return Ok(departments);
        }

        // // GET api/departments/id
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var department = _departmentService.GetDepartment(id);

            if(department == null)
                return NotFound();

            return Ok(department);
        }

        // POST api/departments
        [HttpPost]
        public IActionResult Create([FromBody] CreateUpdateDepartmentDto dto)
        {
            if(dto == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var departmentId = _departmentService.CreateDepartment(dto);

            if(departmentId <= 0)
                return StatusCode(500, "There was an error");

            return Created("Get", new { id = departmentId });
        }

        // PUT api/departments/id
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] CreateUpdateDepartmentDto dto)
        {
            if(dto == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if(!_departmentService.DepartmentExists(id))
                return NotFound();

            _departmentService.UpdateDepartment(id, dto);

            if (!_departmentService.Save())
                return StatusCode(500, "Something went wrong");

            return NoContent();
        }

        // DELETE api/values/id
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(!_departmentService.DepartmentExists(id))
                return NotFound();

            _departmentService.DeleteDepartment(id);

            if (!_departmentService.Save())
                return StatusCode(500, "Something went wrong");

            return NoContent();
        }
    }
}
