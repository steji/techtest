using System.ComponentModel.DataAnnotations;

namespace CompanyService.Api.Models
{
    public class CreateUpdateDepartmentDto
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Location { get; set; }
    }
}