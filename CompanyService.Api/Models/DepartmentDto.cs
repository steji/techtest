using System.Collections.Generic;

namespace CompanyService.Api.Models
{
    public class DepartmentDto : CreateUpdateDepartmentDto
    {
        public int Id { get; set; }
        public IEnumerable<EmployeeDto> Employees { get; set; }
    }
}