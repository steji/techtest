using System.Collections.Generic;

namespace CompanyService.Api.Models
{
    public class EmployeeDto : CreateUpdateEmployeeDto
    {
        public int Id { get; set; }

        public string DepartmentName { get; set; }

        public string DepartmentLocation { get; set; }
    }
}