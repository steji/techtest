using System.ComponentModel.DataAnnotations;

namespace CompanyService.Api.Models
{
    public class CreateUpdateEmployeeDto
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50)]
        public string JobTitle { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}