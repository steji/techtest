using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CompanyService.Api.Entities;
using CompanyService.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace CompanyService.Api.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly ICompanyServiceContext _context;

        public DepartmentService(ICompanyServiceContext context)
        {
            _context = context;
        }

        public int CreateDepartment(CreateUpdateDepartmentDto dto)
        {
            var department = Mapper.Map<Department>(dto);

            _context.Departments.Add(department);

            Save();

            return department.Id;
        }

        public void UpdateDepartment(int id, CreateUpdateDepartmentDto dto)
        {
            var department = _context.Departments.SingleOrDefault(d => d.Id == id);

            Mapper.Map(dto, department);
        }

        public void DeleteDepartment(int id)
        {
            var department = _context.Departments.SingleOrDefault(d => d.Id == id);

            _context.Remove(department);
       }

        public bool DepartmentExists(int id)
        {
            return _context.Departments.Any(d => d.Id == id);
        }

        public DepartmentDto GetDepartment(int id)
        {
            var department = _context.Departments
                .Include(d => d.Employees)
                .SingleOrDefault(d => d.Id == id);

            if(department == null)
                return null;

            var dto = Mapper.Map<DepartmentDto>(department);

            return dto;
        }

        public IEnumerable<DepartmentDto> GetDepartments()
        {
            var departments = _context.Departments
                .Include(d => d.Employees)
                .OrderBy(d => d.Name)
                .ToList();
         
            var dtos = Mapper.Map<IEnumerable<DepartmentDto>>(departments);

            return dtos;
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}