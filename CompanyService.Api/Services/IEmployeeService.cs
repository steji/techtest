using System.Collections.Generic;
using CompanyService.Api.Entities;
using CompanyService.Api.Models;

namespace CompanyService.Api.Services
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeDto> GetEmployees();

        EmployeeDto GetEmployee(int id);

        bool EmployeeExists(int id);

        void DeleteEmployee(int id);

        int CreateEmployee(CreateUpdateEmployeeDto dto);

        void UpdateEmployee(int id, CreateUpdateEmployeeDto dto);

        bool Save();
    }
}