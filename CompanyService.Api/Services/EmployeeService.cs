using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CompanyService.Api.Entities;
using CompanyService.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace CompanyService.Api.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly CompanyServiceContext _context;

        public EmployeeService(CompanyServiceContext context)
        {
            _context = context;
        }

        public int CreateEmployee(CreateUpdateEmployeeDto dto)
        {
            var employee = Mapper.Map<Employee>(dto);

            _context.Employees.Add(employee);

            Save();

            return employee.Id;
        }
        public void UpdateEmployee(int id, CreateUpdateEmployeeDto dto)
        {
            var employee = _context.Employees.SingleOrDefault(e => e.Id == id);

            Mapper.Map(dto, employee);
        }

        public void DeleteEmployee(int id)
        {
            var employee = _context.Employees.SingleOrDefault(e => e.Id == id);

            _context.Remove(employee);
        }

        public bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }

        public EmployeeDto GetEmployee(int id)
        {
            var employee = _context.Employees
                .Include(e => e.Department)
                .SingleOrDefault(e => e.Id == id);

            if(employee == null)
                return null;

            var dto = Mapper.Map<EmployeeDto>(employee);

            return dto;
        }

        public IEnumerable<EmployeeDto> GetEmployees()
        {
            var employees = _context.Employees
                .Include(e => e.Department)
                .OrderBy(e => e.LastName)
                .ThenBy(e => e.FirstName)
                .ToList();

            var dtos = Mapper.Map<IEnumerable<EmployeeDto>>(employees);

            return dtos;
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }

    }
}