using System.Collections.Generic;
using CompanyService.Api.Entities;
using CompanyService.Api.Models;

namespace CompanyService.Api.Services
{
    public interface IDepartmentService
    {
        IEnumerable<DepartmentDto> GetDepartments();

        DepartmentDto GetDepartment(int id);
        
        bool DepartmentExists(int id);

        void DeleteDepartment(int id);

        int CreateDepartment(CreateUpdateDepartmentDto dto);

        void UpdateDepartment(int id, CreateUpdateDepartmentDto dto);

        bool Save();
    }
}