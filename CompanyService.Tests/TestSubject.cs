using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace CompanyService.Tests {
    public abstract class TestSubject<T> {
        protected T Subject;

        protected static DbSet<T1> GetQueryableMockDbSet<T1>(List<T1> sourceList) where T1 : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T1>>();
            dbSet.As<IQueryable<T1>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T1>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T1>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T1>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            dbSet.Setup(d => d.Add(It.IsAny<T1>())).Callback<T1>((s) => sourceList.Add(s));

            return dbSet.Object;
        }
    }
}