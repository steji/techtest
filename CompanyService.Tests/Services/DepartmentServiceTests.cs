﻿using Xunit;
using Moq;
using CompanyService.Api.Services;
using CompanyService.Api.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using CompanyService.Api;

namespace CompanyService.Tests.Services
{
    public class DepartmentServiceTests : TestSubject<DepartmentService>
    {
        //Many more unit tests need writing!
        public DepartmentServiceTests()
        {
            Setup();
        }

        //No [Setup] in XUnit...
        private void Setup()
        {
            var mockContext = new Mock<ICompanyServiceContext>();

            var departments = new List<Department>(){
                new Department {
                    Id = 1,
                    Name = "Test Department",
                    Location = "Test Land",
                    Employees = new List<Employee>(){
                        new Employee {
                            FirstName = "Testy",
                            LastName = "McTesterson",
                            JobTitle = "Tester"
                        }
                    }
                },
                new Department {
                    Id = 2,
                    Name = "Another Department",
                    Location = "From Another Land"
                },
            };

            var dbSet = GetQueryableMockDbSet(departments);

            mockContext.Setup(c => c.Departments)
                .Returns(dbSet);

            Startup.InitialiseAutoMapper();

            Subject = new DepartmentService(mockContext.Object);
        }

        [Fact]
        public void GetDepartment_returns_correctly_mapped_data()
        {
            //Arrange
            Setup();

            //Act
            var result = Subject.GetDepartment(1);

            //Assert
            Assert.Equal(1, result.Id);
            Assert.Equal("Test Department", result.Name);
            Assert.Equal("Test Land", result.Location);
            Assert.Equal("Testy", result.Employees.First().FirstName);
            Assert.Equal("McTesterson", result.Employees.First().LastName);
            Assert.Equal("Tester", result.Employees.First().JobTitle);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void GetDepartment_returns_correct_department_by_id(int id)
        {
            //Arrange
            Setup();

            //Act
            var result = Subject.GetDepartment(id);

            //Assert
            Assert.Equal(id, result.Id);
        }

        [Fact]
        public void DepartmentExists_returns_true_when_exists()
        {
            //Arrange
            Setup();

            //Act
            var result = Subject.DepartmentExists(1);

            //Assert
            Assert.Equal(true, result);
        }

        [Fact]
        public void DepartmentExists_returns_false_when_doesnt_exist()
        {
            //Arrange
            Setup();

            //Act
            var result = Subject.DepartmentExists(-1);

            //Assert
            Assert.Equal(false, result);
        }
    }
}